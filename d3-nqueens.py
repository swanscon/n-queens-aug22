def filter_available_squares(candidate_squares, queen):
    available_squares = []
    for square in candidate_squares:
        if queen[0] == square[0]: continue #same row
        if queen[1] == square[1]: continue #same column
        if sum(square) == sum(queen): continue #same pos diagonal
        if square[0] - square[1] == queen[0] - queen[1]: continue #same neg diagonal
        available_squares.append(square)
    return available_squares

def n_queens(N):
    for column in range(N):
        available_squares = ([(r, c) for r in range(N) for c in range(N)])
        queen = (0, column)
        queens = [queen]
        available_squares = filter_available_squares(available_squares, queen)
        while len(available_squares) > 0:
            queen = available_squares[0]
            queens.append(queen)
            available_squares = filter_available_squares(available_squares, queen)
        if len(queens) == N:
            break
    return queens